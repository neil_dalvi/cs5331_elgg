<?php
/**
 * Elgg dashboard
 *
 * @package Elgg
 * @subpackage Core
 * @author Curverider Ltd
 * @link http://elgg.org/
 */

// Get the Elgg engine
require_once(dirname(dirname(__FILE__)) . "../../engine/start.php");

// Ensure that only logged-in users can see this page
gatekeeper();

// Set context and title
//set_context('stats');
set_page_owner(get_loggedin_userid());
$title = elgg_echo('my-stats');

// wrap intro message in a div
//$intro_message = elgg_view("stat/stats");
//echo "IntroMessage : ".$intro_message;
////echo "file to be loaded : ".$intro_message;
//$intro_message = "hello world";

$options = array (
	
	'entity_type' 		=> 'object',
	'entity_subtype' 	=> 'blog',
	'name' 		=> 'generic_comments',
	'ownerguid' 	=> 3,
	'limit' 	=> 5


);

global $CONFIG;
$user = get_loggedin_user();

//var_dump($user);
echo "User id : ".$user['guid'];

//var_dump($options);

//get_entities_from_annotation_count($entity_type = "", $entity_subtype = "", $name = "", $mdname = '', $mdvalue = '', $owner_guid = 0, $limit = 10, $offset = 0, $orderdir = 'desc', $count = false)
$mostcommentedblog = get_most_commented_blogs("count","object", "blog", "generic_comment", "", "", $user['guid'], 5, 0, "desc", false);
//$mostcommentedblog = get_entities_from_annotation_count($options);

$commentcount_total_daily = count_comments_by_type("count","object","blog","generic_comment","","",$user['guid'],0,0,"","total");
$commentcount_friends_daily = count_comments_by_type("count","object","blog","generic_comment","","",$user['guid'],0,0,"","friends");
$commentcount_self_daily = count_comments_by_type("count","object","blog","generic_comment","","",$user['guid'],0,0,"","self");
$commentcount_others_daily = ($commentcount_total_daily - $commentcount_friends_daily - $commentcount_self_daily);
$countofcomments_weekly;
$countofcomments_monthly;

$countofvisits_daily = get_visit_count_frequency("count","object","blog","visited","","",$user['guid'],0,0,"","daily");
$countofvisits_monthly = get_visit_count_frequency("count","object","blog","visited","","",$user['guid'],0,0,"","monthly");

/*echo "<br/>---------------------------<br/>";
var_dump($countofvisits_daily);
echo "<br/>---------------------------<br/>";
var_dump($countofvisits_monthly);
echo "<br/>---------------------------<br/>";
*/
echo "total ".$commentcount_total_daily;
echo "friends".$commentcount_friends_daily;
echo "self".$commentcount_self_daily;
echo "other".$commentcount_others_daily;


echo "Most Commented Blogs".count($mostcommentedblog);
var_dump($mostcommentedblog);

$intro_message = "
  <script type=\"text/javascript\">
  window.onload = function () {
    var chart = new CanvasJS.Chart(\"chartContainer\",
    {

      title:{
      text: \"Earthquakes - per month\"
      },
	axisX:{      
            valueFormatString: \"DD-MMM\" ,
        },
       data: [
      {
        type: \"line\",

        dataPoints: [";
	foreach($countofvisits_daily as $day) {
		echo "date : ".$day->date;	
		$intro_message .= "{ x: new Date({$day->date}), y: {$day->sum} },";
	}
  $intro_message .=       "]
      }
      ]
    });

    chart.render();
	
	var chart2 = new CanvasJS.Chart(\"chartContainer2\",
    {

      title:{
      text: \"Earthquakes - per month\"
      },
       data: [
      {
        type: \"line\",

        dataPoints: [
        { x: new Date(2012, 01, 1), y: 450 },
        { x: new Date(2012, 01, 8), y: 414 },
        { x: new Date(2012, 01, 15), y: 520 },
        { x: new Date(2012, 01, 22), y: 460 },
        { x: new Date(2012, 01, 29), y: 450 },
        { x: new Date(2012, 02, 7), y: 500 },
        { x: new Date(2012, 02, 14), y: 480 },
        { x: new Date(2012, 02, 21), y: 480 },
        { x: new Date(2012, 02, 28), y: 410 },
        { x: new Date(2012, 03, 4), y: 500 },
        { x: new Date(2012, 03, 11), y: 480 },
        { x: new Date(2012, 03, 18), y: 510 }
        ]
      }
      ]
    });

    chart2.render();
	
	var chart3 = new CanvasJS.Chart(\"chartContainer3\",
    {
      title:{
        text: \"Top 5 most commented blogs\"
      },
      axisY:{
	title: \"#views\",
      },	
      data: [
      {
        type: \"bar\",
        dataPoints: [";
	foreach($mostcommentedblog as $blog) {
		$intro_message .= "{ y: ".$blog['count'].", label: \"".$blog['title']."\"},"  ;
	}
 $intro_message .=       "]
      },
      
      ]
    });

chart3.render();

var chart4 = new CanvasJS.Chart(\"chartContainer4\",
	{
		title:{
			text: \"#Comments by self / friends / other \"
		},
		legend: {
			maxWidth: 350,
			itemWidth: 120
		},
		data: [
		{
			type: \"pie\",
			showInLegend: true,
			legendText: \"{indexLabel}\",
			dataPoints: [";
		$intro_message .= "{ y: ".$commentcount_self_daily.", indexLabel: \"self\" },";
		$intro_message .= "{ y: ".$commentcount_friends_daily.", indexLabel: \"friends\"},";
		$intro_message .= "{ y: ".$commentcount_others_daily.", indexLabel: \"others\"}";
$intro_message .= "]
		}
		]
	});
	chart4.render();
  }
  </script>
 <script type=\"text/javascript\" src=\"http://localhost/elgg/vendors/canvasjs/canvasjs.min.js\"></script></head>
  <div id=\"chartContainer\" style=\"height: 300px; width: 100%;\">
  </div>
  <div id=\"chartContainer2\" style=\"height: 300px; width: 100%;\">
  </div>
  <span>
  <div id=\"chartContainer3\" style=\" height: 300px; width: 100%;\">
  </div>
  <div id=\"chartContainer4\" style=\"height: 300px; width: 100%;\">
  </div>
  </span>
  
";








// Try and get the user from the username and set the page body accordingly
$body = elgg_view_layout("","","",$intro_message);

page_draw($title, $body);
