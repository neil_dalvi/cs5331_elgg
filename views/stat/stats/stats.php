<!DOCTYPE HTML>
<?php
echo "hello";

// Get the Elgg engine
require_once(dirname(dirname(__FILE__)) . "/engine/start.php");

// Ensure that only logged-in users can see this page
gatekeeper();

$options = array (
	
	'entity_type' 		=> 'object',
	'entity_subtype' 	=> 'blog',
	'name' 		=> 'generic_comments',
	'ownerguid' 	=> 3,
	'limit' 	=> 5


);

global $CONFIG;
$user = get_loggedin_user();

//var_dump($user);
echo "User id : ".$user['guid'];

//var_dump($options);

//get_entities_from_annotation_count($entity_type = "", $entity_subtype = "", $name = "", $mdname = '', $mdvalue = '', $owner_guid = 0, $limit = 10, $offset = 0, $orderdir = 'desc', $count = false)
$mostcommentedblog = get_entities_from_annotation_count("object", "blog", "generic_comment", "", "", $user['guid'], 5, 0, "desc", false);
//$mostcommentedblog = get_entities_from_annotation_count($options);

echo "Most Commented Blogs".count($mostcommentedblog);
var_dump($mostcommentedblog);
 
?>
<html>
<head>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {

      title:{
      text: "Earthquakes - per month"
      },
       data: [
      {
        type: "line",

        dataPoints: [
        { x: new Date(2012, 00, 1), y: 450 },
        { x: new Date(2012, 01, 1), y: 414 },
        { x: new Date(2012, 02, 1), y: 520 },
        { x: new Date(2012, 03, 1), y: 460 },
        { x: new Date(2012, 04, 1), y: 450 },
        { x: new Date(2012, 05, 1), y: 500 },
        { x: new Date(2012, 06, 1), y: 480 },
        { x: new Date(2012, 07, 1), y: 480 },
        { x: new Date(2012, 08, 1), y: 410 },
        { x: new Date(2012, 09, 1), y: 500 },
        { x: new Date(2012, 10, 1), y: 480 },
        { x: new Date(2012, 11, 1), y: 510 }
        ]
      }
      ]
    });

    chart.render();
	
	var chart2 = new CanvasJS.Chart("chartContainer2",
    {

      title:{
      text: "Earthquakes - per month"
      },
       data: [
      {
        type: "line",

        dataPoints: [
        { x: new Date(2012, 01, 1), y: 450 },
        { x: new Date(2012, 01, 8), y: 414 },
        { x: new Date(2012, 01, 15), y: 520 },
        { x: new Date(2012, 01, 22), y: 460 },
        { x: new Date(2012, 01, 29), y: 450 },
        { x: new Date(2012, 02, 7), y: 500 },
        { x: new Date(2012, 02, 14), y: 480 },
        { x: new Date(2012, 02, 21), y: 480 },
        { x: new Date(2012, 02, 28), y: 410 },
        { x: new Date(2012, 03, 4), y: 500 },
        { x: new Date(2012, 03, 11), y: 480 },
        { x: new Date(2012, 03, 18), y: 510 }
        ]
      }
      ]
    });

    chart2.render();
	
	var chart3 = new CanvasJS.Chart("chartContainer3",
    {
      title:{
        text: "Olympic Medals of all Times (till 2012 Olympics)"
      },
      data: [
      {
        type: "bar",
        dataPoints: [
        { y: 198, label: "Italy"},
        { y: 201, label: "China"},
        { y: 202, label: "France"},
        { y: 236, label: "Great Britain"},
        { y: 395, label: "Soviet Union"},
        { y: 957, label: "USA"}
        ]
      },
      
      ]
    });

chart3.render();

var chart4 = new CanvasJS.Chart("chartContainer4",
	{
		title:{
			text: "Gaming Consoles Sold in 2012"
		},
		legend: {
			maxWidth: 350,
			itemWidth: 120
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			legendText: "{indexLabel}",
			dataPoints: [
				{ y: 4181563, indexLabel: "PlayStation 3" },
				{ y: 2175498, indexLabel: "Wii" },
				{ y: 3125844, indexLabel: "Xbox 360" },
				{ y: 1176121, indexLabel: "Nintendo DS"},
				{ y: 1727161, indexLabel: "PSP" },
				{ y: 4303364, indexLabel: "Nintendo 3DS"},
				{ y: 1717786, indexLabel: "PS Vita"}
			]
		}
		]
	});
	chart4.render();
  }
  </script>
 <script type="text/javascript" src="http://localhost/elgg/vendors/canvasjs/canvasjs.min.js"></script></head>
<body>
  <div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
  <div id="chartContainer2" style="height: 300px; width: 100%;">
  </div>
  <span>
  <div id="chartContainer3" style="float:left; height: 300px; width: 50%;">
  </div>
  <div id="chartContainer4" class="float:left" style="height: 300px; width: 50%;">
  </div>
  </span>
  
</body>
</html>
