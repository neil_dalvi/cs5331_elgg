<?php
/**
 * Elgg login action
 *
 * @package Elgg
 * @subpackage Core
 * @author Curverider Ltd
 * @link http://elgg.org/
 */
echo "hello to login page";
// Get username and password
$username = get_input('username');
$password = get_input("password");
$persistent = get_input("persistent", false);

$access_token = get_input('access_token');

/**
 * facebook / google / none
 */
$sso_type = get_input("sso");

$ssologin = false;
if($sso_type) {
	if($sso_type === "facebook" || $sso_type === "google" ) {
		$ssologin = true;
	}
}

$url = 'https://graph.facebook.com/me?fields=email,first_name,last_name&access_token='.$access_token;
/*$data = array('fields' => 'email,first_name,last_name', 'access_token' => $access_token);

// use key 'http' even if you send the request to https://...
$options = array(
    'https' => array(
        'header'  => "Content-type: application/json\r\n",
        'method'  => 'GET',
        'content' => http_build_query($data),
    ),
);
$context  = stream_context_create($options);*/
//$result = file_get_contents($url,true, $context);
$result = file_get_contents($url);
if ($result === FALSE) { /* Handle error */ }
$data_back = json_decode($result, true);

var_dump($data_back);

$first_name = $data_back['first_name'];
$last_name = $data_back['last_name'];
$email = $data_back['email'];

$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
$pass = array(); //remember to declare $pass as an array
$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
for ($i = 0; $i < 8; $i++) {
	$n = rand(0, $alphaLength);
	$pass[] = $alphabet[$n];
}
$ssopassword = implode($pass); //turn the array into a string
if($ssologin) {
	$user = get_user_by_emailid($email);
	if(empty($user)) {
		$guid = register_user($first_name, $ssopassword, $first_name." ".$last_name, $email, false, $friend_guid, $invitecode);
		echo "user by this email id exists";
	}
	$result = false;
	$result = login($user, $persistent);

	echo "result of login ".$result;

	if ($result) {
		if (isset($_SESSION['last_forward_from']) && $_SESSION['last_forward_from']) {
			$forward_url = $_SESSION['last_forward_from'];
			unset($_SESSION['last_forward_from']);
			forward($forward_url);
		} else {
			if ( (isadminloggedin()) && (!datalist_get('first_admin_login'))) {
				system_message(elgg_echo('firstadminlogininstructions'));
				datalist_set('first_admin_login', time());
				forward('pg/admin/plugins');
			} else if (get_input('returntoreferer')) {
				forward($_SERVER['HTTP_REFERER']);
			} else {
				forward("pg/dashboard/");
			}
		}
	}
}

if(!$ssologin) {

	// If all is present and correct, try to log in
	$result = false;
	if (!empty($username) && !empty($password)) {
		if ($user = authenticate($username,$password)) {
			$result = login($user, $persistent);
		}
	}

	// Set the system_message as appropriate
	if ($result) {
		system_message(elgg_echo('loginok'));
		if (isset($_SESSION['last_forward_from']) && $_SESSION['last_forward_from']) {
			$forward_url = $_SESSION['last_forward_from'];
			unset($_SESSION['last_forward_from']);
			forward($forward_url);
		} else {
			if ( (isadminloggedin()) && (!datalist_get('first_admin_login'))) {
				system_message(elgg_echo('firstadminlogininstructions'));
				datalist_set('first_admin_login', time());

				forward('pg/admin/plugins');
			} else if (get_input('returntoreferer')) {
				forward($_SERVER['HTTP_REFERER']);
			} else {
				forward("pg/dashboard/");
			}
		}
	} else {
		$error_msg = elgg_echo('loginerror');
		// figure out why the login failed
		if (!empty($username) && !empty($password)) {
			// See if it exists and is disabled
			$access_status = access_get_show_hidden_status();
			access_show_hidden_entities(true);
			if (($user = get_user_by_username($username)) && !$user->validated) {
				// give plugins a chance to respond
				if (!trigger_plugin_hook('unvalidated_login_attempt','user',array('entity'=>$user))) {
					// if plugins have not registered an action, the default action is to
					// trigger the validation event again and assume that the validation
					// event will display an appropriate message
					trigger_elgg_event('validate', 'user', $user);
				}
			} else {
				register_error(elgg_echo('loginerror'));
			}
			access_show_hidden_entities($access_status);
		} else {
			register_error(elgg_echo('loginerror'));
		}
	}
}
